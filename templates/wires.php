<?php

class wires
{
  
  function __construct()
  {
    
  }

  public function header()
  {
    return "<!DOCTYPE html>
    <html>
    <head>
      <meta charset='utf-8'>
      <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
      <title></title>
      <script src='/assets/js/fa-all.min.js'></script>
      <link rel='stylesheet' href='/assets/css/bootstrap.min.css'>
      <script src='/assets/js/jquery-3.3.1.min.js'></script>
      <script src='/assets/js/popper.min.js'></script>
      <script src='/assets/js/bootstrap.min.js'></script>
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,600&amp;subset=latin-ext' rel='stylesheet'>

      <link rel='stylesheet' type='text/css' href='/assets/css/slick.css'/>
      <script type='text/javascript' src='/assets/js/slick.min.js'></script>

      <script src='/assets/js/web.js'></script>
      <link rel='stylesheet' href='/assets/css/style.css'>
    </head>
    <body>";
  }


  public function footer()
  {
    return "
      </body>
    </html>";
  }
}