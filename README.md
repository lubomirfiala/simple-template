# Simple template

Very basic web template for quick prototyping and autonomous landingpages. Useful when you want to code simple website before you take your project seriously enough to use real php framework. 

contains some basic utility
- bootstrap 4.1.0
- jquery 3.3.1.
- fontawesome solid 5.0.13
- slick http://kenwheeler.github.io/slick/ - for carousels
- "ruller" images with pixel grid for better coding

functions:
- primitive regex-based routing
- very primitive templates (repetitive parts of website in "wires class")

the goal is:
- keep included utilities up to date
- keep everything simple as possible 


log
- 0.0.2 - added open sans 300 and 600; corected charset
- 0.0.1 - basic and custom files
  
 


