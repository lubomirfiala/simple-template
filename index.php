<?php

include 'templates/wires.php';


$q = $_SERVER['REQUEST_URI'];
$q = explode('?', $_SERVER['REQUEST_URI'], 2);
$q = $q[0];
$q = trim($q, '\/');


if(isset($q) && preg_match("/page/", $q))
  { include "pages/page.php"; }


elseif( $q == '')  { include "pages/default.php"; }


else { include "pages/404.php"; }
